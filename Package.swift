// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.
//-///////////////////////////////////////////////////////////////////////////////////////

import PackageDescription

//-///////////////////////////////////////////////////////////////////////////////////////

let package = Package(
    name: "HafeleSmartphoneKeySDK",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        .library(
            name: "dialock-ios-sdk-public-full",
            targets: ["dialock-ios-sdk-public-full"]),
    ],
    dependencies: [
        .package(name: "Alamofire", url: "https://github.com/Alamofire/Alamofire.git", "4.9.1"..<"5.0.0"),
        .package(name: "ObjectMapper", url: "https://github.com/tristanhimmelman/ObjectMapper", from: "4.2.0"),
        .package(name: "SwiftyJSON", url: "https://github.com/SwiftyJSON/SwiftyJSON", from: "5.0.1"),
    ],
    targets: [
        .binaryTarget(
            name: "HafeleSmartphoneKeySDK",
            path: "Sources/HafeleSmartphoneKeySDK.xcframework"
        ),
        .target(
            name: "dialock-ios-sdk-public-full",
            dependencies: [
                "HafeleSmartphoneKeySDK",
                "Alamofire",
                "ObjectMapper",
                "SwiftyJSON"
            ]
        )
    ]
)
