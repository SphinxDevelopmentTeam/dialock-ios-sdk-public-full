# Häfele Smartphone Key iOS SDK

## V1.1.10
* removed external dependency CryptoSwift
* include CrypoSwit source
* remove alamofire logger dependency
* endpoint urls without trailing slash

## V1.1.9
* add commands TV9GetAllDeviceCounters, TV9GetDeviceCounter

## V1.1.8
* BUGFIX: ResetDeviceCPU now working
* BUGFIX: PICC CRC Check
*
