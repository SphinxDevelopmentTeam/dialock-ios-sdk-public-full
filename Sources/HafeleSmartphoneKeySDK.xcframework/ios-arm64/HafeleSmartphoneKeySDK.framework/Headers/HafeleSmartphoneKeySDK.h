//
//  HafeleSmartphoneKeySDK.h
//  HafeleSmartphoneKeySDK
//
//  Created by Michał Moryto on 14.09.2016.
//  Copyright © 2016 Hafele. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HafeleSmartphoneKeySDK.
FOUNDATION_EXPORT double HafeleSmartphoneKeySDKVersionNumber;

//! Project version string for HafeleSmartphoneKeySDK.
FOUNDATION_EXPORT const unsigned char HafeleSmartphoneKeySDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HafeleSmartphoneKeySDK/PublicHeader.h>


